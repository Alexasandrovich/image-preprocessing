﻿#include <opencv2/opencv.hpp>
#include <array>
#include <algorithm>
using namespace cv;
using namespace std;

static const int d = 100;
static const cv::Rect2i rc_cell{0, 0, d, d};

void draw_cell(cv::Mat& img, const int32_t x, const int32_t y,
  const cv::Scalar color_ground, const cv::Scalar color_figure) {
  cv::Rect2i rc = rc_cell;
  rc.x += x;
  rc.y += y;
  cv::rectangle(img, rc, color_ground, -1);
  cv::circle(img, {rc.x + rc.width / 2, rc.y + rc.height / 2}, rc.width / 5, color_figure, -1);
}


int main() {
    cv::Mat img_src(200, 300, CV_8UC1);

    draw_cell(img_src, 0, 0, { 255 }, { 127 });
    draw_cell(img_src, d, 0, { 127 }, { 0 });
    draw_cell(img_src, d * 2, 0, { 0 }, { 255 });
    draw_cell(img_src, 0, d, { 0 }, { 127 });
    draw_cell(img_src, d, d, { 255 }, { 0 });
    draw_cell(img_src, d * 2, d, { 127 }, { 255 });


    imwrite("lab04.src.png", img_src);
    Mat f1, f2, f3;
    Mat kernel1(Mat::zeros(2, 2, CV_8SC1));
    Mat kernel2 = kernel1.clone();
    kernel1.at<int8_t>(0, 0) = 1.;  kernel1.at<int8_t>(1, 1) = -1.;
    kernel2.at<int8_t>(0, 1) = 1.;  kernel2.at<int8_t>(1, 0) = -1.;
    filter2D(img_src, f1, CV_32F, kernel1, Point(-1, -1), 0, BORDER_REFLECT);
    filter2D(img_src, f2, CV_32F, kernel2, Point(-1, -1), 0, BORDER_REFLECT);
    pow(f1.mul(f1) + f2.mul(f2), 0.5, f3);
    f1 = ((f1 + 255) / 2);  f2 = ((f2 + 255) / 2);
    f1.convertTo(f1, CV_8UC1);
    f2.convertTo(f2, CV_8UC1);
    f3.convertTo(f3, CV_8UC1);
    imwrite("lab04.viz_dx.png", f1);
    imwrite("lab04.viz_dy.png", f2);
    imwrite("lab04.viz_gradmod.png", f3);

}
