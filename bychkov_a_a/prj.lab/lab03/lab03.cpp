﻿#include <opencv2/opencv.hpp>
#include <iostream>
#include <opencv2/ximgproc.hpp>

using namespace std;
using namespace cv;

void show_hist(array<int, 256>& collected_pix, string name_hist){

    Mat histogram(Size(120, 256), CV_8U, Scalar(0));

    double max_in_hist = 0;
    for(size_t pix_freq = 0; pix_freq < collected_pix.size(); pix_freq++)
    {
        if(collected_pix[pix_freq] > max_in_hist)
        {
            max_in_hist = collected_pix[pix_freq];
        }
    }

    for(size_t i = 0; i < 256; i++)
    {
        for(int j = 0; j < collected_pix[i]; j++)
        {
            histogram.at<unsigned char>(250 - cvRound(double(j / max_in_hist * 120)), i) = 150;
        }
    }
    histogram.col(59).setTo(255);
    resize(histogram, histogram, Size(histogram.rows * 2, histogram.cols * 2));
    imwrite(name_hist, histogram);
}

array<int, 256> collect_pix_freq(Mat& input_image)
{
    array<int, 256> collected_pix = {};
    for(size_t i = 0; i < input_image.rows; i++)
    {
        for(size_t j = 0; j < input_image.cols; j++)
        {
            collected_pix[input_image.at<unsigned char>(i, j)]++;
        }
    }
    return collected_pix;
}

vector<int> getLUT() {
   vector<int> LUT(256);
   for(size_t i = 0; i < LUT.size(); i++)
   {
       if(i < 80)
       {
           LUT[i] = 255;
       }
       else if(i >= 80 && i <= 200)
       {
           LUT[i] = 150;
       }
       else if(i > 200)
       {
           LUT[i] = 50;
       }
   }
   return LUT;
}

int otsuThreshold(Mat image, array<int, 256>& collected_pix)
{
  // Посчитаем минимальную и максимальную яркость всех пикселей
  double min, max;
  minMaxLoc(image, &min, &max);
  int m = 0;
  int n = 0;
  for (int t = min; t <= max; t++)
  {
    m += t * collected_pix[t];
    n += collected_pix[t];
  }

  float maxSigma = -1; // Максимальное значение межклассовой дисперсии
  int threshold = 0; // Порог

  int alpha1 = 0;
  int beta1 = 0;

  // t пробегается по всем возможным значениям порога
  for (int t = min; t < max ; t++)
  {
    alpha1 += t * collected_pix[t];
    beta1 += collected_pix[t];

    // Считаем вероятность класса 1.
    float w1 = (float)beta1 / n;

    // a = a1 - a2, где a1, a2 - средние арифметические для классов 1 и 2
    float a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);

    // считаем sigma
    float sigma = w1 * (1 - w1) * a * a;

    // Если sigma больше текущей максимальной, то обновляем maxSigma и порог
    if (sigma > maxSigma)
    {
      maxSigma = sigma;
      threshold = t;
    }
  }

  // Не забудем, что порог отсчитывался от min, а не от нуля
  threshold += min;

  // порог посчитан, возвращаем его наверх
  return threshold;
}

void draw_lut_func(vector<int>& lut)
{
    Mat lut_func(Size(257, 257), CV_8U, Scalar(0));
    for(size_t pix = 0; pix < 256; pix++)
    {
        int value = lut[pix];
        lut_func.at<unsigned char>(value, pix) = 255;
    }
    flip(lut_func, lut_func, 0);
    imwrite("lab03.hist.lut.png", lut_func);
}

Mat change_img(Mat input_image)
{
    vector<int> LUT = getLUT();
    draw_lut_func(LUT);

    cv::LUT(input_image, LUT, input_image);
    imwrite("lab03.src.lut.png", input_image);
    return input_image;
}

vector<int> getLUT_bin(int thr) {
   vector<int> LUT(256);
   for(size_t i = 0; i < LUT.size(); i++)
   {
       LUT[i] = i >= thr ? 255 : 0;
   }
   return LUT;
}

Mat change_img_bin(Mat input_image, int thr, string save_path)
{
    vector<int> LUT = getLUT_bin(thr);
    cv::LUT(input_image, LUT, input_image);
    imwrite(save_path, input_image);
    return input_image;
}

void use_clahe(Ptr<CLAHE> clahe, Mat& image, const string save_path_hist, const string save_path_img)
{
    Mat clahe_img;
    clahe->apply(image, clahe_img);
    array<int, 256> collected_pix_with_change_clahe = collect_pix_freq(clahe_img);
    show_hist(collected_pix_with_change_clahe, save_path_hist);
    imwrite(save_path_img, clahe_img);
}

Mat use_niblack(Mat image_bin, string save_path)
{
    Mat res;
    cv::ximgproc::niBlackThreshold(image_bin, res, 255, THRESH_BINARY, 35, -1);
    image_bin = res.clone();
    imwrite(save_path, res);
    return res;
}

Mat use_morph(Mat img)
{
    Mat morphology_image;
    Mat full_morphology_image;
    Mat element = cv::getStructuringElement(MORPH_ERODE, Size(2, 2));
    cv::morphologyEx(img, morphology_image, MORPH_OPEN, element);
    full_morphology_image.push_back(morphology_image);
    imwrite("lab03.morph.png", full_morphology_image);

    return img;

}

int main()
{
    // функцию распределения для eq hist тоже представлять в виде lookup-table
    // как бахнуть Оцу через фильтр-гаусса и прочее

    Mat image = imread("../../testdata/bin.png", CV_LOAD_IMAGE_GRAYSCALE);
    if(!image.empty())
    {

        // построение гистограммы и её вывод
        array<int, 256> collected_pix_no_change = collect_pix_freq(image);
        show_hist(collected_pix_no_change, "lab03.hist.src.png");

        // гистограммное преобразование и вывод
        Mat image_change = change_img(image.clone());
        array<int, 256> collected_pix_with_change = collect_pix_freq(image_change);
        show_hist(collected_pix_with_change, "lab03.lut.png");

        // CLAHE
        Ptr<CLAHE> clahe = cv::createCLAHE();
        use_clahe(clahe, image, "lab03.hist.clahe.1.png", "lab03.clahe.1.png");

        clahe->setClipLimit(4);
        clahe->setTilesGridSize(Size(4, 4));
        use_clahe(clahe, image, "lab03.hist.clahe.2.png", "lab03.clahe.2.png");

        clahe->setClipLimit(40);
        clahe->setTilesGridSize(Size(32, 32));
        use_clahe(clahe, image, "lab03.hist.clahe.3.png", "lab03.clahe.3.png");

        // бинаризация
        Mat image_bin = imread("../../testdata/bin.png", IMREAD_GRAYSCALE);
        // Оцу
        int thr = otsuThreshold(image_bin, collected_pix_no_change);
        Mat global_img = change_img_bin(image_bin, thr, "lab03.bin.global.png");

        // Ниблек
        Mat local_bin = use_niblack(image_bin, "lab03.bin.local.png");

        // Морфология
        Mat morph = use_morph(local_bin);

        Mat binary_mask;
        double alpha = 0.7; double beta = (1.0 - alpha);
        addWeighted(global_img, alpha, image, beta, 0.0, binary_mask);
        imwrite("lab03.mask.png", binary_mask);

        waitKey(0);

    }else
    {
        cout << "Empty image" << endl;
        return -1;
    }

}
