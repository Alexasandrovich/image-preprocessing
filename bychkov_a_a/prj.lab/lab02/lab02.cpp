﻿#include <iostream>
#include <string>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

struct Image
{
    Mat source_clr;
    Mat source_gr;
    vector<Mat> colors;
};

void compressing(Mat source_img)
{
    imwrite("cubes_65.jpg", source_img, { (int)IMWRITE_JPEG_QUALITY, 65 });
    imwrite("cubes_95.jpg", source_img, { (int)IMWRITE_JPEG_QUALITY, 95 });
}

vector<Mat> make_empty(int& not_empty, Mat color)
{
    Mat empty = Mat::zeros(color.rows, color.cols, CV_8UC1);
    if(not_empty == 0) {
        vector<Mat> channels = { color, empty, empty};
        return channels;
    }else if(not_empty == 1) {
        vector<Mat> channels = { empty, color, empty};
        return channels;
    }else if(not_empty == 2)  {
       vector<Mat> channels = { empty, empty, color};
       return channels;
    }
}

Mat evaluate_channels(Image& obj_1, Image& obj_2, Image& start_obj)
{
    vector<Mat> channels_1;
    split(obj_1.source_clr, channels_1);
    obj_1.colors = channels_1;

    vector<Mat> channels_2;
    split(obj_2.source_clr, channels_2);
    obj_2.colors = channels_2;

    vector<Mat> channels_start;
    split(start_obj.source_clr, channels_start);
    start_obj.colors = channels_start;

    cvtColor(obj_1.source_gr, obj_1.source_gr, COLOR_GRAY2RGB);
    cvtColor(obj_2.source_gr, obj_2.source_gr, COLOR_GRAY2RGB);
    cvtColor(start_obj.source_gr, start_obj.source_gr, COLOR_GRAY2RGB);

    // gray2bgr
    for(int i = 0; i < 3; i++)
    {
        // obj_1
        Mat color_1;
        auto one_color_1 = make_empty(i, obj_1.colors[i]);
        Mat channel_1[] = {one_color_1[0], one_color_1[1], one_color_1[2]};
        merge(channel_1, 3, color_1);
        obj_1.colors[i] = color_1;

        // obj_2
        Mat color_2;
        auto one_color_2 = make_empty(i, obj_2.colors[i]);
        Mat channel_2[] = {one_color_2[0], one_color_2[1], one_color_2[2]};
        merge(channel_2, 3, color_2);
        obj_2.colors[i] = color_2;

        // start_obj
        Mat color_start;
        auto one_color_start = make_empty(i, start_obj.colors[i]);
        Mat channel_start[] = {one_color_start[0], one_color_start[1], one_color_start[2]};
        merge(channel_start, 3, color_start);
        start_obj.colors[i] = color_start;
    }

    // поканальные различия в цвете между сжатыми картинками
    Mat diff_color = (obj_1.colors[0] - obj_2.colors[0]) * 20;
    for(size_t color = 1; color < obj_1.colors.size(); color++)
    {
        Mat diff = (obj_1.colors[color] - obj_2.colors[color]) * 20;
        hconcat(diff_color, diff, diff_color);
    }
//    imwrite("lab02_diff_color.png", diff_color);

    // поканальные различия в цвете между ИСХОДНОЙ картинкой и одной из сжатой
    Mat diff_color_start_1 = (obj_1.colors[0] - start_obj.colors[0]) * 20;
    for(size_t color = 1; color < obj_1.colors.size(); color++)
    {
        Mat diff = (obj_1.colors[color] - start_obj.colors[color]) * 20;
        hconcat(diff_color_start_1, diff, diff_color_start_1);
    }
//    imwrite("lab02_diff_color_start_1.png", diff_color_start_1);

    // поканальные различия в цвете между ИСХОДНОЙ картинкой и одной из сжатой
    Mat diff_color_start_2 = (obj_2.colors[0] - start_obj.colors[0]) * 20;
    for(size_t color = 1; color < obj_1.colors.size(); color++)
    {
        Mat diff = (obj_2.colors[color] - start_obj.colors[color]) * 20;
        hconcat(diff_color_start_2, diff, diff_color_start_2);
    }
//    imwrite("lab02_diff_color_start_2.png", diff_color_start_2);

    // в сером
    Mat diff_gray = (channels_1[0] - channels_2[0]) * 50;
    for(size_t gray = 1; gray < channels_1.size(); gray++)
    {
        Mat diff = (channels_1[gray] - channels_2[gray]) * 50;
        hconcat(diff_gray, diff, diff_gray);
    }
//    imwrite("lab02_diff_gray.png", diff_gray);

    // в сером между ОРИГИНАЛОМ и другим
    Mat diff_gray_start_1 = (channels_1[0] - channels_start[0]) * 50;
    for(size_t gray = 1; gray < channels_1.size(); gray++)
    {
        Mat diff = (channels_1[gray] - channels_start[gray]) * 50;
        hconcat(diff_gray_start_1, diff, diff_gray_start_1);
    }
//    imwrite("lab02_diff_gray_start_1.png", diff_gray_start_1);

    // в сером между ОРИГИНАЛОМ и другим
    Mat diff_gray_start_2 = (channels_2[0] - channels_start[0]) * 50;
    for(size_t gray = 1; gray < channels_1.size(); gray++)
    {
        Mat diff = (channels_2[gray] - channels_start[gray]) * 50;
        hconcat(diff_gray_start_2, diff, diff_gray_start_2);
    }
//    imwrite("lab02_diff_gray_start_2.png", diff_gray_start_2);

    // gray_diff оригинал
    Mat diff_brightness_gray_start_1 = (start_obj.source_gr - obj_1.source_gr) * 20;
    // gray_diff оригинал
    Mat diff_brightness_gray_start_2 = (start_obj.source_gr - obj_2.source_gr) * 20;
    // gray_diff
    Mat diff_brightness_gray = (obj_1.source_gr - obj_2.source_gr) * 20;
    hconcat(diff_brightness_gray_start_1, diff_brightness_gray_start_2, diff_brightness_gray_start_1);
    hconcat(diff_brightness_gray_start_1, diff_brightness_gray, diff_brightness_gray_start_1);
    imwrite("grey_diff.png", diff_brightness_gray_start_1);

    // color_diff оригинал
    Mat diff_brightness_color_start_1 = (start_obj.source_clr - obj_1.source_clr) * 20;
    // color_diff оригинал
    Mat diff_brightness_color_start_2 = (start_obj.source_clr - obj_2.source_clr) * 20;
    // color_diff
    Mat diff_brightness_color = (obj_1.source_clr - obj_2.source_clr) * 20;
    hconcat(diff_brightness_color_start_1, diff_brightness_color_start_2, diff_brightness_color_start_1);
    hconcat(diff_brightness_color_start_1, diff_brightness_color, diff_brightness_color_start_1);
    imwrite("brightness_diff.png", diff_brightness_color_start_1);

    return diff_color;
}

void comparing(Image& obj_1, Image& obj_2, Image& start_obj)
{
    evaluate_channels(obj_1, obj_2, start_obj);
}

int main() {
    Mat image = imread("/home/alex/compressed.png");
    resize(image, image, cv::Size(), 0.2, 0.2);
    if(!image.empty())
    {
        // сжимаем
        compressing(image);
        Image obj_1, obj_2, start_obj;
        start_obj.source_clr = image.clone();
        cvtColor(start_obj.source_clr, start_obj.source_gr, CV_RGB2GRAY);

        obj_1.source_gr  = imread("cubes_65.jpg", cv::IMREAD_GRAYSCALE);
        obj_1.source_clr = imread("cubes_65.jpg", cv::IMREAD_COLOR);
        obj_2.source_gr  = imread("cubes_95.jpg", cv::IMREAD_GRAYSCALE);
        obj_2.source_clr  = imread("cubes_95.jpg", cv::IMREAD_COLOR);

        // сравниваем
        comparing(obj_1, obj_2, start_obj);


    }else{
        cout << "Empty image - check path" << endl;
    }
    cv::waitKey(0);
    return 0;
}
